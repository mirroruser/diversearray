import java.util.Arrays;

public class DiverseArray {
    public static int arraySum(int[] arr) {
        return Arrays.stream(arr).sum();
    }

    public static int[] rowSums(int[][] arr2D) {
        int[] out = new int[arr2D.length];

        for (int i = 0; i < out.length; i++) {
            out[i] = DiverseArray.arraySum(arr2D[i]);
        }
        return out;
    }

    public static boolean isDiverse(int[][] arr2D) {
        int[] source = DiverseArray.rowSums(arr2D);

        for (int i = 0; i < source.length; i++) {
            for (int j = i + 1; j < source.length; j++) {
                if (source[i] == source[j]) {
                    return false;
                }
            }
        }
        return true;
    }
}
